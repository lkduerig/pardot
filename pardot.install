<?php
/**
 * @file
 * Install/uninstall taks and updates.
 */

/**
 * Implementation of hook_schema().
 */
function pardot_schema() {
  $schema = array();
  $schema['pardot_submissions'] = array(
    'description' => 'Pardot Form submissions',
    'primary key' => array('sid'),
    'fields' => array(
      'sid' => array(
        // Internal unique identifier.
        'description' => 'Webform submission id',
        'type' => 'serial',
        'size' => 'normal',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'form_nid' => array(
        // Associated Form Id
        'description' => 'Webform nid that generated this post',
        'type' => 'int',
        'size' => 'normal',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'status' => array(
        // Form Posted status.
        'description' => 'Pardot option name',
        'type' => 'varchar',
        'size' => 'normal',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'submitted' => array(
        // Post Time
        'description' => 'Timestamp of form submission',
        'type' => 'int',
        'size' => 'normal',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'data' => array(
        // Form Contents
        'description' => 'Form values and other data',
        'type' => 'text',
        'size' => 'normal', /* 16KB in mySql */
        'not null' => TRUE,
        'serialize' => TRUE,
      ),
    ),
  );

  $schema['pardot_webform'] = array(
    'description' => 'Webform Settings',
    'primary key' => array('nid'),
    'unique keys' => array(),
    'indexes' => array(
      'pardot_webform_active' => array('is_active'),
    ),
    'fields' => array(
      'nid' => array(
        // Webform Node Id
        'description' => 'Node Id',
        'type' => 'int',
        'size' => 'normal',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'url' => array(
        'description' => 'Pardot post url',
        'type' => 'varchar',
        'size' => 'normal',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'is_active' => array(
        // Is this webform Pardot enabled
        'description' => 'Whether this form is Pardot active',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
        'unsigned' => TRUE,
      ),
      'data' => array(
        'description' => 'Extra data to be stored with the field',
        'type' => 'text',
        'size' => 'normal', /* 16KB in mySql */
        'not null' => TRUE,
        'serialize' => TRUE,
      ),
    ),
  );

  $schema['pardot_scoring'] = array(
    'description' => 'Scoring settings',
    'primary key' => array('scoring_id'),
     'indexes' => array(),
    'fields' => array(
      'scoring_id' => array(
        // Internal unique identifier.
        'type' => 'serial',
        'size' => 'normal',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'path' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'score' => array(
        'description' => 'Pardot score for a given page',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
        'unsigned' => FALSE,
      ),
      'lang' => array(
        'description' => 'Language for Pardot score for a given page',
        'type' => 'varchar',
        'size' => 'normal',
        'length' => 5,
        'not null' => TRUE,
        'default' => 'en',
        
      ),

    ),
  );

  $schema['pardot_campaign'] = array(
    'description' => 'Campaign settings',
    'indexes' => array(),
    'fields' => array(
      'campaign_id' => array(
        'description' => 'Pardot campaign identifier.',
        // Internal unique identifier.
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'name' => array(
        // Form Posted status.
        'description' => 'Human readable campaign name',
        'type' => 'varchar',
        'size' => 'normal',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'paths' => array(
        'description' => 'A list of paths associated with the campaign',
        'type' => 'text',
        'size' => 'normal', /* 16KB in mySql */
        'not null' => TRUE,
        'serialize' => FALSE,
      ),
      'lang' => array(
        'description' => 'Language for Pardot score for a given page',
        'type' => 'varchar',
        'size' => 'normal',
        'length' => 5,
        'not null' => TRUE,
        'default' => 'en',
        
      ),
    ),
  );
  return $schema;
}


/**
 * Implementation of hook_install()
 */
function pardot_install() {
}


/**
 * Implementation of hook_uninstall()
 */
function pardot_uninstall() {
}

/**
 * Update Pardot Campaign and pardot Scoring tables
 */
function pardot_update_7100(&$sandbox) {
  //Add language fileds to pardot campaign and pardot scoring tables.
  db_add_field('pardot_campaign', 'lang', array('type' => 'varchar', 'size' => 'normal', 'length' => 5, 'not null' => TRUE, 'default' => 'en'));
  db_add_field('pardot_scoring', 'lang', array('type' => 'varchar', 'size' => 'normal', 'length' => 5, 'not null' => TRUE, 'default' => 'en'));
  db_change_field('pardot_submissions', 'form_nid_field', 'form_nid',
  array('description' => 'Webform nid that generated this post', 'type' => 'int', 'size' => 'normal', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0));

  // Remove primary and unique keys for storing duplicate records for diffrent 
  // langauges. 
  db_drop_primary_key('pardot_campaign');
  db_drop_unique_key('pardot_scoring');

  return t('The pardot tables were updated.');
}
