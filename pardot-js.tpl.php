<?php 
if ($pardot_a_id):
  print "\n" . 'piAId = ' . $pardot_a_id . ';';
else:
   print "\n" . 'piAId = "";';
endif;

if ($pardot_c_id):
  print "\n" . 'piCId = ' . $pardot_c_id . ';';
else:
   print "\n" . 'piCId = "";';
endif;
?>
<?php if ($score): ?>
piPoints = "<?php print $score; ?>";
<?php endif; ?>

(function() {
function async_load(){
  var s = document.createElement('script'); s.type = 'text/javascript';
  s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
  var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
}
if(window.attachEvent) { window.attachEvent('onload', async_load); }
else { window.addEventListener('load', async_load, false); }
})();
